﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using ClipboardMonitor;
using TPLinkHTTPLib;
using System.Runtime.InteropServices;

namespace DailerMainUi
{
    public partial class FormMain : Form
    {
        private delegate void uidlg(int stat, string errInfo);
        private Thread dialThrd;
        private Process subAppPcs;
        //private string[] txtBoxValue = new string[5];
        //private int lbSelectedIndex = 0;
        private DailerConfig.Config cfg;
        [DllImport("User32.dll", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("User32.dll", EntryPoint = "FindWindowEx")]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("User32.dll", EntryPoint = "SendMessage", CharSet = CharSet.Unicode)]
        private static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);
        [DllImport("User32.dll", EntryPoint = "ShowWindow")]
        private static extern int ShowWindow(IntPtr hWnd, int showCmd);
        //[DllImport("User32.dll", CharSet = CharSet.Unicode)]
        //public static extern IntPtr PostMessage(IntPtr hwnd, int wMsg, IntPtr wParam, IntPtr lParam);
        public const int WM_SETTEXT = 0x000C;
        public const int WM_CLICK = 0x00F5;
        public const int CB_SETCURSEL = 0x014E;
        public FormMain()
        {
            InitializeComponent();
            cfg = new DailerConfig.Config(Environment.CurrentDirectory + "\\Config.ini");
        }
        private void trdUpdateUI(int status,string errInfo = "")
        {
            if (InvokeRequired)
            {
                uidlg ud = new uidlg(trdUpdateUI);
                Invoke(ud, new object[] { status, errInfo });
            }
            else
            {
                if (status < 0)
                    labelProgramStatus.Text = "遇到错误";
                switch (status)
                {
                    case -1:
                        MessageBox.Show("无法找到算号器！本软件可能不完整。\r\n" + errInfo, "Error");
                        break;
                    case -2:
                        MessageBox.Show("无法连接到路由器地址，请检查网络连接！" + errInfo, "Error");
                        break;
                    case -3:
                        MessageBox.Show("无法启动算号器！请检查计算机设置。\r\n" + errInfo, "Error");
                        break;
                    case -4:
                        MessageBox.Show("获取到的剪切板数据不正确，请尝试关闭自动操作。\r\n" + errInfo, "Error");
                        break;
                    case -5:
                        MessageBox.Show("无法操作目标程序，请尝试关闭自动操作。\r\n" + errInfo, "Error");
                        break;
                    case -6:
                        MessageBox.Show("长时间没有获取到剪切板数据。" + errInfo, "Error");
                        break;
                    case 0:
                        labelProgramStatus.Text = "等待用户操作";
                        break;
                    case 1:
                        labelProgramStatus.Text = "正在监控系统剪切板";
                        break;
                    case 2:
                        labelProgramStatus.Text = "正在连接路由器拨号";
                        break;
                    case 3:
                        labelProgramStatus.Text = "拨号请求已经发出";
                        break;
                    case 4:
                        labelProgramStatus.Text = "正在启动算号器";
                        break;
                    default:
                        MessageBox.Show("不明的错误：未接收到可识别的状态代码。\r\n" + errInfo, "Error");
                        break;
                }
            }
        }
        private void dailReady()
        {
            trdUpdateUI(1);
            subAppPcs = new Process();
            if (File.Exists("SubApp1.exe")) subAppPcs.StartInfo.FileName = "SubApp1.exe";
            else
            {
                try
                {
                    File.Copy("SubApp1.bin", Environment.GetEnvironmentVariable("TEMP") + @"\CMCCTRouterDailer_Sub1.exe", true);
                    subAppPcs.StartInfo.FileName = Environment.GetEnvironmentVariable("TEMP") + @"\CMCCTRouterDailer_Sub1.exe";
                }
                catch { trdUpdateUI(-1); return; }
            }
            try {
                subAppPcs.Start();
                if (cfg.autoSendAction)
                    subAppPcs.WaitForInputIdle();
            } catch { trdUpdateUI(-1); return; }
            string ispUsername;
            if (cfg.autoSendAction)
            {
                IntPtr windowPtr = FindWindow("#32770", "");
                if(windowPtr.Equals(IntPtr.Zero)) { trdUpdateUI(-5);return; }
                ShowWindow(windowPtr, 2);
                IntPtr accountInputPtr = FindWindowEx(windowPtr, (IntPtr)0, "RichEdit20A", "");
                if (accountInputPtr.Equals(IntPtr.Zero)) { trdUpdateUI(-5); return; }
                SendMessage(accountInputPtr, WM_SETTEXT, IntPtr.Zero, cfg.ispUsername);
                IntPtr passwordInputPtr = FindWindowEx(windowPtr, accountInputPtr, "RichEdit20A", "");
                if (passwordInputPtr.Equals(IntPtr.Zero)) { trdUpdateUI(-5); return; }
                SendMessage(passwordInputPtr, WM_SETTEXT, IntPtr.Zero, cfg.ispPassword);
                IntPtr versionSelectCBPtr = FindWindowEx(windowPtr, IntPtr.Zero, "ComboBox", null);
                if (versionSelectCBPtr.Equals(IntPtr.Zero)) { trdUpdateUI(-5); return; }
                SendMessage(versionSelectCBPtr, CB_SETCURSEL, (IntPtr)8, null);
                IntPtr calcButtonPtr = FindWindowEx(windowPtr, IntPtr.Zero, "Button", "C");
                if (calcButtonPtr.Equals(IntPtr.Zero)) { trdUpdateUI(-5); return; }
                SendMessage(calcButtonPtr, WM_CLICK, IntPtr.Zero, null);
                subAppPcs.WaitForInputIdle();
                //Thread.Sleep(500);
                ispUsername = Clipboard.GetText();
                if (!ispUsername.StartsWith("~ghca")) { trdUpdateUI(-4); return; }
            }
            else
            {
                ClicpboardMonitorAction cma = new ClicpboardMonitorAction();
                ispUsername = cma.getNewStringStartWith("~ghca", 60);
                if (!ispUsername.StartsWith("~ghca")) { trdUpdateUI(-6); return; }
            }
            string routerDomainIp = cfg.routerAddress;
            int routerPort = 80;
            int routerPortStringOffset;
            try { subAppPcs.CloseMainWindow(); }catch { }
            if (0 < (routerPortStringOffset = cfg.routerAddress.IndexOf(':')))
            {
                Int32.TryParse(cfg.routerAddress.Substring(routerPortStringOffset + 1), out routerPort);
                routerDomainIp = cfg.routerAddress.Substring(0, routerPortStringOffset);
            }
            Router tpRouter = new Router(routerDomainIp, routerPort, cfg.routerAdminUsername, cfg.routerAdminPassword);
            trdUpdateUI(2);
            tpRouter.sendPPPoEConfig(ispUsername, cfg.ispPassword, (cfg.isAutoDisconnect ? cfg.autoDisconnectTime.ToString() : "0"));
            tpRouter.reConnect();
            trdUpdateUI(3);
        }
        private void updateConfig()
        {
            cfg.routerAddress = textBoxRouterAddress.Text;
            cfg.routerAdminUsername = textBoxRouterAdminAccount.Text;
            cfg.routerAdminPassword = textBoxRouterAdminPassword.Text;
            cfg.ispUsername = textBoxIspAccountUsername.Text;
            cfg.ispPassword = textBoxIspAccountPassword.Text;
            cfg.isAutoDisconnect = checkBoxAutoDisconnect.Checked;
            cfg.autoDisconnectTime = Convert.ToInt32(numericUpDownAutoDisconnectMinutes.Value);
            cfg.autoRunCalcTool = checkBoxAutoRunCalcToolNextTime.Checked;
            cfg.autoSendAction = checkBoxAutoCalc.Checked;
        }
        private void buttonExitProgram_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void buttonShowQAndAForm_Click(object sender, EventArgs e)
        {
            FormQAndA subFormQAndA = new FormQAndA(this);
            Hide();
            subFormQAndA.Show();
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            try { dialThrd.Abort(); }catch { }
            if (checkBoxRememberMySettings.Checked)
            {
                updateConfig();
                cfg.saveConfig();
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            cfg.readConfig();
            textBoxRouterAddress.Text = cfg.routerAddress;
            textBoxRouterAdminAccount.Text = cfg.routerAdminUsername;
            textBoxRouterAdminPassword.Text = cfg.routerAdminPassword;
            textBoxIspAccountUsername.Text = cfg.ispUsername;
            textBoxIspAccountPassword.Text = cfg.ispPassword;
            checkBoxAutoDisconnect.Checked = cfg.isAutoDisconnect;
            numericUpDownAutoDisconnectMinutes.Value = cfg.autoDisconnectTime;
            checkBoxAutoRunCalcToolNextTime.Checked = cfg.autoRunCalcTool;
            checkBoxAutoCalc.Checked = cfg.autoSendAction;
            checkBoxAutoDisconnect_CheckedChanged(sender, e);
            if (cfg.autoRunCalcTool) buttonRunCalcTool_Click(sender, e);
        }

        private void checkBoxAutoDisconnect_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAutoDisconnect.Checked)
                numericUpDownAutoDisconnectMinutes.Enabled = true;
            else
                numericUpDownAutoDisconnectMinutes.Enabled = false;
        }

        private void buttonRunCalcTool_Click(object sender, EventArgs e)
        {
            updateConfig();
            try { dialThrd.Abort(); } catch { }
            dialThrd = new Thread(new ThreadStart(dailReady));
            dialThrd.SetApartmentState(ApartmentState.STA);
            dialThrd.Start();
        }
    }
}
