﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DailerMainUi
{
    public partial class FormQAndA : Form
    {
        private Form parentForm;
        public FormQAndA(Form pForm)
        {
            parentForm = pForm;
            InitializeComponent();
        }

        private void buttonCloseQAndAForm_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormQAndA_FormClosed(object sender, FormClosedEventArgs e)
        {
            parentForm.Show();
        }
    }
}
