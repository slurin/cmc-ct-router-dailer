﻿namespace DailerMainUi
{
    partial class FormMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.labelRouterAddress = new System.Windows.Forms.Label();
            this.labelRouterAdminAccount = new System.Windows.Forms.Label();
            this.labelRouterAdminPassword = new System.Windows.Forms.Label();
            this.labelIspAccountPassword = new System.Windows.Forms.Label();
            this.labelAutoDisconnectMinutes = new System.Windows.Forms.Label();
            this.labelProgramStatus = new System.Windows.Forms.Label();
            this.textBoxRouterAddress = new System.Windows.Forms.TextBox();
            this.textBoxRouterAdminAccount = new System.Windows.Forms.TextBox();
            this.textBoxRouterAdminPassword = new System.Windows.Forms.TextBox();
            this.textBoxIspAccountPassword = new System.Windows.Forms.TextBox();
            this.checkBoxAutoDisconnect = new System.Windows.Forms.CheckBox();
            this.checkBoxAutoRunCalcToolNextTime = new System.Windows.Forms.CheckBox();
            this.checkBoxRememberMySettings = new System.Windows.Forms.CheckBox();
            this.numericUpDownAutoDisconnectMinutes = new System.Windows.Forms.NumericUpDown();
            this.buttonShowQAndAForm = new System.Windows.Forms.Button();
            this.buttonRunCalcTool = new System.Windows.Forms.Button();
            this.buttonExitProgram = new System.Windows.Forms.Button();
            this.checkBoxAutoCalc = new System.Windows.Forms.CheckBox();
            this.labelIspAccountUsername = new System.Windows.Forms.Label();
            this.textBoxIspAccountUsername = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoDisconnectMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // labelRouterAddress
            // 
            this.labelRouterAddress.AutoSize = true;
            this.labelRouterAddress.Location = new System.Drawing.Point(12, 9);
            this.labelRouterAddress.Name = "labelRouterAddress";
            this.labelRouterAddress.Size = new System.Drawing.Size(65, 12);
            this.labelRouterAddress.TabIndex = 0;
            this.labelRouterAddress.Text = "路由器地址";
            // 
            // labelRouterAdminAccount
            // 
            this.labelRouterAdminAccount.AutoSize = true;
            this.labelRouterAdminAccount.Location = new System.Drawing.Point(12, 36);
            this.labelRouterAdminAccount.Name = "labelRouterAdminAccount";
            this.labelRouterAdminAccount.Size = new System.Drawing.Size(101, 12);
            this.labelRouterAdminAccount.TabIndex = 1;
            this.labelRouterAdminAccount.Text = "路由器管理员账号";
            // 
            // labelRouterAdminPassword
            // 
            this.labelRouterAdminPassword.AutoSize = true;
            this.labelRouterAdminPassword.Location = new System.Drawing.Point(12, 63);
            this.labelRouterAdminPassword.Name = "labelRouterAdminPassword";
            this.labelRouterAdminPassword.Size = new System.Drawing.Size(101, 12);
            this.labelRouterAdminPassword.TabIndex = 2;
            this.labelRouterAdminPassword.Text = "路由器管理员密码";
            // 
            // labelIspAccountPassword
            // 
            this.labelIspAccountPassword.AutoSize = true;
            this.labelIspAccountPassword.Location = new System.Drawing.Point(12, 117);
            this.labelIspAccountPassword.Name = "labelIspAccountPassword";
            this.labelIspAccountPassword.Size = new System.Drawing.Size(77, 12);
            this.labelIspAccountPassword.TabIndex = 3;
            this.labelIspAccountPassword.Text = "电信宽带密码";
            // 
            // labelAutoDisconnectMinutes
            // 
            this.labelAutoDisconnectMinutes.AutoSize = true;
            this.labelAutoDisconnectMinutes.Location = new System.Drawing.Point(174, 170);
            this.labelAutoDisconnectMinutes.Name = "labelAutoDisconnectMinutes";
            this.labelAutoDisconnectMinutes.Size = new System.Drawing.Size(29, 12);
            this.labelAutoDisconnectMinutes.TabIndex = 5;
            this.labelAutoDisconnectMinutes.Text = "分钟";
            // 
            // labelProgramStatus
            // 
            this.labelProgramStatus.AutoSize = true;
            this.labelProgramStatus.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelProgramStatus.Location = new System.Drawing.Point(12, 258);
            this.labelProgramStatus.Name = "labelProgramStatus";
            this.labelProgramStatus.Size = new System.Drawing.Size(77, 12);
            this.labelProgramStatus.TabIndex = 8;
            this.labelProgramStatus.Text = "等待用户操作";
            // 
            // textBoxRouterAddress
            // 
            this.textBoxRouterAddress.Location = new System.Drawing.Point(119, 6);
            this.textBoxRouterAddress.Name = "textBoxRouterAddress";
            this.textBoxRouterAddress.Size = new System.Drawing.Size(100, 21);
            this.textBoxRouterAddress.TabIndex = 100;
            this.textBoxRouterAddress.Text = "192.168.1.1";
            // 
            // textBoxRouterAdminAccount
            // 
            this.textBoxRouterAdminAccount.Location = new System.Drawing.Point(119, 33);
            this.textBoxRouterAdminAccount.Name = "textBoxRouterAdminAccount";
            this.textBoxRouterAdminAccount.Size = new System.Drawing.Size(100, 21);
            this.textBoxRouterAdminAccount.TabIndex = 101;
            this.textBoxRouterAdminAccount.Text = "admin";
            // 
            // textBoxRouterAdminPassword
            // 
            this.textBoxRouterAdminPassword.Location = new System.Drawing.Point(119, 60);
            this.textBoxRouterAdminPassword.Name = "textBoxRouterAdminPassword";
            this.textBoxRouterAdminPassword.PasswordChar = '*';
            this.textBoxRouterAdminPassword.Size = new System.Drawing.Size(100, 21);
            this.textBoxRouterAdminPassword.TabIndex = 102;
            this.textBoxRouterAdminPassword.Text = "admin";
            // 
            // textBoxIspAccountPassword
            // 
            this.textBoxIspAccountPassword.Location = new System.Drawing.Point(119, 114);
            this.textBoxIspAccountPassword.Name = "textBoxIspAccountPassword";
            this.textBoxIspAccountPassword.PasswordChar = '*';
            this.textBoxIspAccountPassword.Size = new System.Drawing.Size(100, 21);
            this.textBoxIspAccountPassword.TabIndex = 104;
            // 
            // checkBoxAutoDisconnect
            // 
            this.checkBoxAutoDisconnect.AutoSize = true;
            this.checkBoxAutoDisconnect.Location = new System.Drawing.Point(12, 166);
            this.checkBoxAutoDisconnect.Name = "checkBoxAutoDisconnect";
            this.checkBoxAutoDisconnect.Size = new System.Drawing.Size(108, 16);
            this.checkBoxAutoDisconnect.TabIndex = 106;
            this.checkBoxAutoDisconnect.Text = "无使用自动断线";
            this.checkBoxAutoDisconnect.UseVisualStyleBackColor = true;
            this.checkBoxAutoDisconnect.CheckedChanged += new System.EventHandler(this.checkBoxAutoDisconnect_CheckedChanged);
            // 
            // checkBoxAutoRunCalcToolNextTime
            // 
            this.checkBoxAutoRunCalcToolNextTime.AutoSize = true;
            this.checkBoxAutoRunCalcToolNextTime.Location = new System.Drawing.Point(12, 188);
            this.checkBoxAutoRunCalcToolNextTime.Name = "checkBoxAutoRunCalcToolNextTime";
            this.checkBoxAutoRunCalcToolNextTime.Size = new System.Drawing.Size(108, 16);
            this.checkBoxAutoRunCalcToolNextTime.TabIndex = 108;
            this.checkBoxAutoRunCalcToolNextTime.Text = "自动启动算号器";
            this.checkBoxAutoRunCalcToolNextTime.UseVisualStyleBackColor = true;
            // 
            // checkBoxRememberMySettings
            // 
            this.checkBoxRememberMySettings.AutoSize = true;
            this.checkBoxRememberMySettings.Location = new System.Drawing.Point(12, 210);
            this.checkBoxRememberMySettings.Name = "checkBoxRememberMySettings";
            this.checkBoxRememberMySettings.Size = new System.Drawing.Size(96, 16);
            this.checkBoxRememberMySettings.TabIndex = 109;
            this.checkBoxRememberMySettings.Text = "记住我的设置";
            this.checkBoxRememberMySettings.UseVisualStyleBackColor = true;
            // 
            // numericUpDownAutoDisconnectMinutes
            // 
            this.numericUpDownAutoDisconnectMinutes.Location = new System.Drawing.Point(126, 165);
            this.numericUpDownAutoDisconnectMinutes.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownAutoDisconnectMinutes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownAutoDisconnectMinutes.Name = "numericUpDownAutoDisconnectMinutes";
            this.numericUpDownAutoDisconnectMinutes.Size = new System.Drawing.Size(42, 21);
            this.numericUpDownAutoDisconnectMinutes.TabIndex = 107;
            this.numericUpDownAutoDisconnectMinutes.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            // 
            // buttonShowQAndAForm
            // 
            this.buttonShowQAndAForm.Location = new System.Drawing.Point(12, 232);
            this.buttonShowQAndAForm.Name = "buttonShowQAndAForm";
            this.buttonShowQAndAForm.Size = new System.Drawing.Size(63, 23);
            this.buttonShowQAndAForm.TabIndex = 112;
            this.buttonShowQAndAForm.Text = "使用说明";
            this.buttonShowQAndAForm.UseVisualStyleBackColor = true;
            this.buttonShowQAndAForm.Click += new System.EventHandler(this.buttonShowQAndAForm_Click);
            // 
            // buttonRunCalcTool
            // 
            this.buttonRunCalcTool.Location = new System.Drawing.Point(144, 206);
            this.buttonRunCalcTool.Name = "buttonRunCalcTool";
            this.buttonRunCalcTool.Size = new System.Drawing.Size(75, 23);
            this.buttonRunCalcTool.TabIndex = 110;
            this.buttonRunCalcTool.Text = "启动算号器";
            this.buttonRunCalcTool.UseVisualStyleBackColor = true;
            this.buttonRunCalcTool.Click += new System.EventHandler(this.buttonRunCalcTool_Click);
            // 
            // buttonExitProgram
            // 
            this.buttonExitProgram.Location = new System.Drawing.Point(155, 235);
            this.buttonExitProgram.Name = "buttonExitProgram";
            this.buttonExitProgram.Size = new System.Drawing.Size(64, 23);
            this.buttonExitProgram.TabIndex = 111;
            this.buttonExitProgram.Text = "退出程序";
            this.buttonExitProgram.UseVisualStyleBackColor = true;
            this.buttonExitProgram.Click += new System.EventHandler(this.buttonExitProgram_Click);
            // 
            // checkBoxAutoCalc
            // 
            this.checkBoxAutoCalc.AutoSize = true;
            this.checkBoxAutoCalc.Location = new System.Drawing.Point(12, 144);
            this.checkBoxAutoCalc.Name = "checkBoxAutoCalc";
            this.checkBoxAutoCalc.Size = new System.Drawing.Size(108, 16);
            this.checkBoxAutoCalc.TabIndex = 105;
            this.checkBoxAutoCalc.Text = "自动操作算号器";
            this.checkBoxAutoCalc.UseVisualStyleBackColor = true;
            // 
            // labelIspAccountUsername
            // 
            this.labelIspAccountUsername.AutoSize = true;
            this.labelIspAccountUsername.Location = new System.Drawing.Point(12, 90);
            this.labelIspAccountUsername.Name = "labelIspAccountUsername";
            this.labelIspAccountUsername.Size = new System.Drawing.Size(77, 12);
            this.labelIspAccountUsername.TabIndex = 112;
            this.labelIspAccountUsername.Text = "电信宽带账号";
            // 
            // textBoxIspAccountUsername
            // 
            this.textBoxIspAccountUsername.Location = new System.Drawing.Point(119, 87);
            this.textBoxIspAccountUsername.Name = "textBoxIspAccountUsername";
            this.textBoxIspAccountUsername.Size = new System.Drawing.Size(100, 21);
            this.textBoxIspAccountUsername.TabIndex = 103;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 277);
            this.Controls.Add(this.textBoxIspAccountUsername);
            this.Controls.Add(this.labelIspAccountUsername);
            this.Controls.Add(this.checkBoxAutoCalc);
            this.Controls.Add(this.buttonExitProgram);
            this.Controls.Add(this.buttonRunCalcTool);
            this.Controls.Add(this.buttonShowQAndAForm);
            this.Controls.Add(this.numericUpDownAutoDisconnectMinutes);
            this.Controls.Add(this.checkBoxRememberMySettings);
            this.Controls.Add(this.checkBoxAutoRunCalcToolNextTime);
            this.Controls.Add(this.checkBoxAutoDisconnect);
            this.Controls.Add(this.textBoxIspAccountPassword);
            this.Controls.Add(this.textBoxRouterAdminPassword);
            this.Controls.Add(this.textBoxRouterAdminAccount);
            this.Controls.Add(this.textBoxRouterAddress);
            this.Controls.Add(this.labelProgramStatus);
            this.Controls.Add(this.labelAutoDisconnectMinutes);
            this.Controls.Add(this.labelIspAccountPassword);
            this.Controls.Add(this.labelRouterAdminPassword);
            this.Controls.Add(this.labelRouterAdminAccount);
            this.Controls.Add(this.labelRouterAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "电信校园路由器拨号辅助工具";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoDisconnectMinutes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRouterAddress;
        private System.Windows.Forms.Label labelRouterAdminAccount;
        private System.Windows.Forms.Label labelRouterAdminPassword;
        private System.Windows.Forms.Label labelIspAccountPassword;
        private System.Windows.Forms.Label labelAutoDisconnectMinutes;
        private System.Windows.Forms.Label labelProgramStatus;
        private System.Windows.Forms.TextBox textBoxRouterAddress;
        private System.Windows.Forms.TextBox textBoxRouterAdminAccount;
        private System.Windows.Forms.TextBox textBoxRouterAdminPassword;
        private System.Windows.Forms.TextBox textBoxIspAccountPassword;
        private System.Windows.Forms.CheckBox checkBoxAutoDisconnect;
        private System.Windows.Forms.CheckBox checkBoxAutoRunCalcToolNextTime;
        private System.Windows.Forms.CheckBox checkBoxRememberMySettings;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoDisconnectMinutes;
        private System.Windows.Forms.Button buttonShowQAndAForm;
        private System.Windows.Forms.Button buttonRunCalcTool;
        private System.Windows.Forms.Button buttonExitProgram;
        private System.Windows.Forms.CheckBox checkBoxAutoCalc;
        private System.Windows.Forms.Label labelIspAccountUsername;
        private System.Windows.Forms.TextBox textBoxIspAccountUsername;
    }
}

