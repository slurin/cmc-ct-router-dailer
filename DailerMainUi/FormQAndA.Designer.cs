﻿namespace DailerMainUi
{
    partial class FormQAndA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQAndA));
            this.richTextBoxQAndAContents = new System.Windows.Forms.RichTextBox();
            this.buttonCloseQAndAForm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBoxQAndAContents
            // 
            this.richTextBoxQAndAContents.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBoxQAndAContents.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBoxQAndAContents.Cursor = System.Windows.Forms.Cursors.Default;
            this.richTextBoxQAndAContents.Location = new System.Drawing.Point(12, 12);
            this.richTextBoxQAndAContents.Name = "richTextBoxQAndAContents";
            this.richTextBoxQAndAContents.ReadOnly = true;
            this.richTextBoxQAndAContents.ShortcutsEnabled = false;
            this.richTextBoxQAndAContents.Size = new System.Drawing.Size(268, 249);
            this.richTextBoxQAndAContents.TabIndex = 100;
            this.richTextBoxQAndAContents.Text = resources.GetString("richTextBoxQAndAContents.Text");
            // 
            // buttonCloseQAndAForm
            // 
            this.buttonCloseQAndAForm.Location = new System.Drawing.Point(224, 267);
            this.buttonCloseQAndAForm.Name = "buttonCloseQAndAForm";
            this.buttonCloseQAndAForm.Size = new System.Drawing.Size(57, 23);
            this.buttonCloseQAndAForm.TabIndex = 101;
            this.buttonCloseQAndAForm.Text = "关闭";
            this.buttonCloseQAndAForm.UseVisualStyleBackColor = true;
            this.buttonCloseQAndAForm.Click += new System.EventHandler(this.buttonCloseQAndAForm_Click);
            // 
            // FormQAndA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 301);
            this.Controls.Add(this.buttonCloseQAndAForm);
            this.Controls.Add(this.richTextBoxQAndAContents);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormQAndA";
            this.ShowIcon = false;
            this.Text = "使用说明";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormQAndA_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBoxQAndAContents;
        private System.Windows.Forms.Button buttonCloseQAndAForm;
    }
}